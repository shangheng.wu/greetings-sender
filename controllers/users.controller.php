<?php

/**
 * Created by PhpStorm.
 * User: shanghengwu
 * Date: 16/03/2017
 * Time: 9:04 AM
 */
require_once (ROOT.DS."models".DS."user.php");
class UsersController extends Controller {
    function __construct(array $data = array()) {
        parent::__construct($data);
        $this->model = new User();
    }

    public function index(){
        Session::setFlash("Login successfully");
        if ( $_POST ){ // move to send() in users.controller
            try{
                $msg = new Message();
                $msg_id = $msg->save($_POST);
                $this->send_email($msg_id, $_POST);
                Session::setFlash("Thank you! your message was sent successfully!");
            } catch (Exception $ex){
                Session::setFlash("Failed to send message");
            }
        }
    }

    public function admin_index(){
        $this->data["users"] = $this->model->getAllUsers();
    }


    public function admin_edit(){
        if ( $_POST ){
            $id = isset($_POST['id']) ? $_POST['id'] : null;
            $result = $this->model->update($_POST, $id);

            if ( $result ){
                Session::setFlash('Users was updated.');
            } else {
                Session::setFlash('Error');
            }
            Router::redirect('/admin/users/');
        }

        if ( isset($this->params[0]) ){
            $this->data['user'] = $this->model->getById($this->params[0]);
        } else {
            Session::setFlash('Wrong user id');
            Router::redirect('/admin/users/');
        }
    }

    public function admin_delete(){
        syslog(LOG_INFO, "deleting user with id: ".$this->params[0]);
        if ( isset($this->params[0]) ) {
            $id = $this->params[0];
            if ($this->model->delete($id)) {
                Session::setFlash('User was deleted.');
            } else {
                Session::setFlash('User was NOT found.');
            }
        }
        Router::redirect('/admin/users/');

    }

    private function send_email($msg_id, $data){
        $receiver_name = $data['receiver_name'];
        $receiver_email = $data['receiver_email'];
        $url = getenv('DOMAIN')."messages/index/".$msg_id;
        $body = "Hi {$receiver_name}, <br/>There is a message for you: {$url} </br> Please enter the following secret key to read: <b>{$data['secret_key']}</b>";

        $mail = new PHPMailer();
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = getenv('EMAIL_HOST');  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = getenv('EMAIL_ACCOUNT');            // SMTP username
        $mail->Password = getenv('EMAIL_PASSWORD');                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;

        $mail->setFrom('shangheng.w@gmail.com', $data['sender_name']);
        $mail->addAddress($receiver_email, $receiver_name);     // Add a recipient

        $mail->isHTML(true);                                  // Set email format to HTML

        $mail->Subject = 'Greeting from ' . $data['sender_name'];
        $mail->Body    = $body;
        $mail->AltBody = $body;
//        $mail->SMTPDebug = 3; // show error details

        if(!$mail->send()) {
            syslog(LOG_ERR, 'Mailer Error: ' . $mail->ErrorInfo);
            return true;
        } else {
            syslog(LOG_DEBUG, 'Message has been sent');
            return false;
        }
    }

    public function admin_login(){
        if ( $_POST && isset($_POST['login']) && isset($_POST['password'])){
            $user = $this->model->getByLogin( $_POST['login'] );
            $hash = md5(Config::get('salt').$_POST['password']);

            if ( $user && $user['is_active'] && $hash == $user['password'] && $user['role']=='admin'){
                Session::set("login", $user["login"]);
                Session::set("role", $user["role"]);
            }

            Router::redirect("/admin/");
        }
    }

    public function admin_logout(){
        Session::destroy();
        Router::redirect("/admin/");
    }

    public function login(){
        if ( $_POST && isset($_POST['login']) && isset($_POST['password'])){
            $user = $this->model->getByUsername( $_POST['login'] );
            $hash = md5(Config::get('salt').$_POST['password']);

            if ( $user && $user['is_active'] && $hash == $user['password'] && $user['role']=='user'){
                Session::set("id", $user["id"]);
                Session::set("login", $user["login"]);
                Session::set("role", $user["role"]);
                Router::redirect("/users/");
            } else {
                Session::setFlash("Login failed. Please check your username/password again.");
            }
        }
    }

    public function logout(){
        Session::destroy();
        Router::redirect("/");
    }

    public function register(){
        Session::destroy();
        session_start();
        if ( $_POST && isset($_POST['login']) && isset($_POST['password'])){
            $username = $_POST['login'];
            $hash = md5(Config::get('salt').$_POST['password']);

            try{
                $id = $this->model->create($username, $hash);
                Session::set("id", $id);
                Session::set("login", $username);
                Session::set("role", "user");
                Router::redirect("/users/");
            } catch (PDOException $ex ){
//                Router::redirect("/users/register");
                Session::setFlash("Duplicated username. Please use another one.");
            }
        }
    }
}