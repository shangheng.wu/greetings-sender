<?php
include_once (ROOT.DS."models".DS."message.php");
include_once (ROOT.DS."libs".DS."controller.php");
include_once ('vendor/phpmailer/phpmailer/PHPMailerAutoload.php');

class MessagesController extends Controller{

    public function __construct(array $data = array()){
        parent::__construct($data);
        $this->model = new Message();
    }

    public function index(){
        $this->data["msg_id"] = $this->params[0];
    }

    public function admin_index() {
        $this->data = $this->model->getList();
        if ($_POST) {
            if (isset($_POST["resent_msg_id"])) {
                $msg_id = $_POST["resent_msg_id"];

                if ( $this->admin_resend($msg_id) ){
                    try{
                        Session::setFlash("Message #{$msg_id} was resent successfully!");
                    } catch (Exception $e){
                        Session::setFlash("Failed to send message");
                    }
                } else {
                    Session::setFlash("Message #{$msg_id} was not found!");
                }
            } else {
                Session::setFlash("Invalid message id");
            }
        }
    }

    private function admin_resend($msg_id){
        $msg = $this->model->getById($msg_id);
        if ( isset($msg) ){
            $this->send_email($msg_id, $msg);
            return true;
        } else {
            return false;
        }
    }

    public function show(){
        if ( $_POST && isset($_POST['message_id']) && isset($_POST['key'])){
            $message_id = $_POST['message_id'];
            $secret_key = $_POST['key'];
            $message = $this->model->getById($message_id);
            if ( $message && $message['secret_key'] == $secret_key){
                $this->data['message'] = $message['message'];
                $this->data['receiver_name'] = $message['receiver_name'];
                Session::setFlash("The secret key entered is correct!");
            } else {
                Session::setFlash("The secret key entered is wrong!");
//                Router::redirect("/pages");
            }
        } else {
            Router::redirect("/messages");
        }
    }

    private function send_email($msg_id, $data){
        $receiver_name = $data['receiver_name'];
        $receiver_email = $data['receiver_email'];
        $url = getenv('DOMAIN')."messages".DS."index".DS.$msg_id;
        $body = "Hi {$receiver_name}, <br/>There is a message for you: {$url} </br> Please enter the secret key to read: {$data['secret_key']}";

        $mail = new PHPMailer();
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = getenv('EMAIL_HOST');  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = getenv('EMAIL_ACCOUNT');            // SMTP username
        $mail->Password = getenv('EMAIL_PASSWORD');                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;

        $mail->setFrom('shangheng.w@gmail.com', $data['sender_name']);
        $mail->addAddress($receiver_email, $receiver_name);     // Add a recipient

        $mail->isHTML(true);                                  // Set email format to HTML

        $mail->Subject = 'Greeting from ' . $data['sender_name'];
        $mail->Body    = $body;
        $mail->AltBody = $body;
//        $mail->SMTPDebug = 3; // show error details

        if(!$mail->send()) {
            syslog(LOG_ERR, 'Mailer Error: ' . $mail->ErrorInfo);
            return true;
        } else {
            syslog(LOG_DEBUG, 'Message has been sent');
            return false;
        }
    }

}