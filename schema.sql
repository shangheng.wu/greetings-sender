CREATE DATABASE IF NOT EXISTS `yahoo`;
USE `yahoo`;


DROP TABLE IF EXISTS `messages`;
CREATE TABLE `messages` (
  `id` smallint(3) unsigned NOT NULL AUTO_INCREMENT,
  `secrety_key` varchar(100) NOT NULL,
  `receiver_name` varchar(30) NOT NULL,
  `receiver_email` varchar(30) NOT NULL,
  `sender_id` smallint(5) unsigned NOT NULL,
  'message' text
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(30) NOT NULL,
  `role` varchar(10) NOT NULL DEFAULT 'admin',
  `password` char(64) NOT NULL,
  `is_active` tinyint(1) unsigned DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE KEY('login')
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
