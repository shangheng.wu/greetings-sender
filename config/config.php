<?php
/**
 * Created by PhpStorm.
 * User: shanghengwu
 * Date: 16/03/2017
 * Time: 10:47 AM
 */
Config::set("site_name", "Greeting Sender");
Config::set("routes", array(
    'default' => '',
    'admin' => 'admin_',
));


Config::set("default_route", "default");
Config::set("default_controller", "messages");
Config::set("default_action", "index");


Config::set("default_header", "greeting sender");
//Config::set("default_path", "/");

if (getenv('SALT'))
    Config::set("salt", getenv('SALT'));
else
    Config::set("salt", "some_random_string");
