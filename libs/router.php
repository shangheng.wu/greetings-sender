<?php
include("libs/config.php");

/**
 * Created by PhpStorm.
 * User: shanghengwu
 * Date: 16/03/2017
 * Time: 4:55 AM
 */
class Router
{
    protected $uri;
    protected $controller;
    protected $action;
    protected $params;
    protected $route;
    protected $method_prefix;

    /**
     * @param string $route
     */
    public function setRoute($route)
    {
        $this->route = $route;
    }

    /**
     * @param string $method_prefix
     */
    public function setMethodPrefix($method_prefix)
    {
        $this->method_prefix = $method_prefix;
    }

    /**
     * @return string
     */
    public function getRoute()
    {
        return $this->route;
    }
    /**
     * @return string
     */
    public function getMethodPrefix()
    {
        return $this->method_prefix;
    }

    /**
     * @return string
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * @return mixed|null
     */
    public function getController()
    {
        return $this->controller;
    }

    /**
     * @return mixed|null
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @return mixed
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @param string $uri
     */
    public function setUri($uri)
    {
        $this->uri = $uri;
    }

    /**
     * @param mixed|null $controller
     */
    public function setController($controller)
    {
        $this->controller = $controller;
    }

    /**
     * @param mixed|null $action
     */
    public function setAction($action)
    {
        $this->action = $action;
    }

    /**
     * @param mixed $params
     */
    public function setParams($params)
    {
        $this->params = $params;
    }

    function route(){
        $method = $this->getHttpMethod();
        $url = $this->getUrl();
        if (array_key_exists($method, self::$routing_table) && array_key_exists($url, self::$routing_table[$method])){
                $routing = self::$routing_table[$method][$url];
                $controllerName = $routing["controller"];
                $actionName = $routing["action"];
                $controller = new $controllerName();
                return $controller->$actionName("aa");
        }
        echo "not existing url<br>";

        return false;
    }

    public function __construct($uri){
        $this->uri = urldecode(trim($uri, '/'));
//        print_r("router was called with uri: " . $this->uri . "<br>");

        // Get defaults
        $routes = Config::get("routes");
        $this->route = Config::get("default_route");
        $this->method_prefix = isset($routes[$this->route]) ? $routes[$this->route] : '';
        $this->controller = Config::get("default_controller");
        $this->action = Config::get("default_action");

        $uri_parts = explode("?", $this->uri);

        // Grab path like /lng/controller/action/param1/param2/.../...
        $path = $uri_parts[0];
        $path_parts = explode('/', $path);

        if (count($path_parts)){
            // Get routes at first element
            if (in_array(strtolower(current($path_parts)), array_keys($routes))){
                $this->route = strtolower(current($path_parts));
                $this->method_prefix = isset($routes[$this->route]) ? $routes[$this->route] : '';
                array_shift($path_parts);
            }

            // Get controller - next element
            if (current($path_parts)){
                $this->controller = strtolower(current($path_parts));
                array_shift($path_parts);
            }

            // Get action
            if (current($path_parts)){
                $this->action = strtolower(current($path_parts));
                array_shift($path_parts);
            }

            // Get params - all the rest
            if (count($path_parts)==1 && $path_parts[0]==""){
                $this->params = array();
            }else {
                $this->params = $path_parts;
            }

        }
    }

    public static function redirect($location){
        header("Location: $location");
    }
}