<?php

/**
 * Created by PhpStorm.
 * User: shanghengwu
 * Date: 16/03/2017
 * Time: 11:26 AM
 */
class Config
{
    protected static $setting = array(); //associative array

    public static function get($key){
        return isset(self::$setting[$key]) ? self::$setting[$key] : null;
    }

    public static function set($key, $value){
        self::$setting[$key] = $value;
    }


}