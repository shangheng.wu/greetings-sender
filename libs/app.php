<?php

/**
 * Created by PhpStorm.
 * User: shanghengwu
 * Date: 16/03/2017
 * Time: 10:50 AM
 */
include_once (ROOT.DS."controllers".DS."messages.controller.php");
include_once (ROOT.DS."libs".DS."view.class.php");
include_once(ROOT . DS . "libs" . DS . "mypdo.php");
include_once (ROOT.DS."controllers".DS."users.controller.php");

class App {
    protected static $router;

    public static $pdo;

    /**
     * @return mixedx
     */
    public static function getRouter(){
        return self::$router;
    }

    public static function run($uri){
        self::$router = new Router($uri);
        self::$pdo = new MyPDO();

        $controller_class = ucfirst(self::$router->getController()).'Controller'; // ucfirst: return: Returns a string with the first character of str capitalized, if that character is alphabetic
        $controller_method = strtolower(self::$router->getMethodPrefix().self::$router->getAction());

        $layout = self::$router->getRoute();
        if ( $layout == 'admin' && Session::get('role') != 'admin'){
            if ( $controller_method != "admin_login"){
                Router::redirect("/admin/users/login"); //redirect browser to avoid infinite loop
            }
        }

        if ( $controller_class == 'UsersController' && $controller_method == 'index' && Session::get('role') != 'user'){
            Router::redirect("/users/login/"); //redirect browser to login
        }

        // Calling controller's method
        if (class_exists($controller_class)){
            $controller_object = new $controller_class();
        } else {
            throw new Exception("class not found");
        }

        syslog(LOG_INFO, "calling action: {$controller_class}->{$controller_method}");
        if (method_exists($controller_object, $controller_method)){
            // Controller's action may return a view path
            $view_path = $controller_object->$controller_method();

            syslog(LOG_INFO, "supposed to call calling view: {$view_path}");
            $view_object = new View($controller_object->getData(), $view_path);
            $content = $view_object->render();
        } else {
            throw new Exception('Method '.$controller_method.' of class '.$controller_class.' does not exist.');
        }

        $layout_path = VIEWS_PATH.DS.$layout.'.html';
        $layout_view_object = new View(compact("content"), $layout_path); // compact:  Create array containing variables and their values
        echo $layout_view_object->render();
    }
}