<?php
/**
 * Created by PhpStorm.
 * User: shanghengwu
 * Date: 16/03/2017
 * Time: 11:56 AM
 */

require_once(ROOT.DS.'config'.DS.'config.php');


/*
 * Not working on google app engine since google app engine is not
 * using Apache as its web server but Darwin(maybe?)
 */
function autoload($class_name){
    echo "running __autoload inside init.php";
    $lib_path = ROOT.DS.'libs'.DS.strtolower($class_name).'.php';
    $controllers_path = ROOT.DS.'controllers'.DS.str_replace('controller', '', strtolower($class_name)).'.php';
    $model_path = ROOT.DS.'models'.DS.strtolower($class_name).'.php';

    if (file_exists($lib_path)){
        require_once($lib_path);
    } elseif (file_exists($controllers_path)){
        require_once($controllers_path);
    } elseif (file_exists($model_path)){
        require_once ($model_path);
    } else {
        throw new Exception('Failed to include class: '.$class_name);
    }
}
