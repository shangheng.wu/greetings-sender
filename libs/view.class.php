<?php
/**
 * Created by PhpStorm.
 * User: shanghengwu
 * Date: 16/03/2017
 * Time: 5:16 PM
 */


class View {

    protected $data; // to store data from controller to view
    protected $path; // path to the current view file

    protected static function getDefaultViewPath(){
        $router = App::getRouter();
        if ( !router ){
            return false;
        }

        $controller_dir = $router->getController();
        $template_name = $router->getMethodPrefix().$router->getAction().".html";

        return VIEWS_PATH.DS.$controller_dir.DS.$template_name;
    }

    public function __construct($data = array(), $path=null){
        if ( !$path ){
            $path = self::getDefaultViewPath();
        }
        if ( !file_exists($path) ){
            throw new Exception("Template file is not found in path: ".$path);
        }
        syslog(LOG_INFO, "actually calling view: {$path}");

        $this->path = $path;
        $this->data = $data;
    }

    public function render(){
        $data = $this->data;
        ob_start(); // Turn on output buffering
        include($this->path);
        $content = ob_get_clean();

        return $content;
    }
}