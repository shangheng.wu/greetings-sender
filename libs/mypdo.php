<?php

/**
 * Created by PhpStorm.
 * User: shanghengwu
 * Date: 14/03/2017
 * Time: 3:56 AM
 */

class MyPDO {
    public $conn;
    private $_dsn = '';
    private $_hostname = '';
    private $_db_name = '';
    private $_username = '';
    private $_password = '';
    private $_encode = 'utf8';
    private $_stmt = '';
    private $_data = [];
    private $_last_insert_id;

    /**
     * MyPDO constructor.
     */
    function __construct() {
        $this->_hostname = getenv('MYSQL_HOST');
        $this->_username = getenv('MYSQL_USER');
        $this->_password = getenv('MYSQL_PASSWORD');
        $this->_db_name = getenv('MYSQL_DB_NAME');
        $this->_dsn = "mysql:host={$this->_hostname};dbname={$this->_db_name}";

        try{
            $this->conn = new PDO($this->_dsn, $this->_username, $this->_password, array(
                    PDO::ATTR_PERSISTENT => TRUE,
                    PDO::MYSQL_ATTR_SSL_KEY  => 'mysql/ssl/client-key.pem',
                    PDO::MYSQL_ATTR_SSL_CERT => 'mysql/ssl/client-cert.pem',
                    PDO::MYSQL_ATTR_SSL_CA   => 'mysql/ssl/server-ca.pem')
            );
            $this->_setEncode();
            $this->conn->setAttribute(
                PDO::ATTR_ERRMODE, //錯誤提示
                PDO::ERRMODE_EXCEPTION //顯示警告錯誤
            );
        } catch (Exception $ex){
            print_r($ex);
        }
    }

    function __set($name, $value){
        $this->_data[$name] = $value;
    }

    function __get($name){
        if (isset($this->_data[$name])){
            return $this->_data[$name];
        }
    }
    private function _setEncode(){
        $this->conn->query("SET NAMES '{$this->_encode}'");

    }

    function bindQuery($sql, array $bind = []){
        $this->_stmt = $this->conn->prepare($sql);
        $this->_bind = ($bind);
        $this->_stmt->execute();
        return $this->_stmt->fetchAll();
    }

    private function _bind($bind){
        foreach($bind as $key => $value ){
            $this->_stmt->bindValue($key, $value, is_numeric($value)? PDO::PARAM_INT : PDO::PARAM_STR);
//            echo ("bind: {$key} => {$value}<br>");
        }
    }

    function error(){
        $error = $this->_stmt->errorInfo();
        echo 'errorCode:'.$error[0].'<br>';
        echo 'errorString'.$error[2].'<br>';
    }

    function getData(){
        return $this->_data;
    }

    function insert($table, array $param = []){
        $data = array_merge($this->_data, $param);
        $columns = array_keys($data);
        $values = [];
        $bind_data = [];
        foreach ($data as $key => $value){
            $values[] = ":{$key}";
            $bind_data[":{$key}"] = $value;
        }
        $sql = "INSERT INTO {$table} (" . implode(',', $columns) . ") VALUES (" . implode(',', $values) . ")";
        $this->_stmt = $this->conn->prepare($sql);
        $this->_bind($bind_data);
        $this->_stmt->execute();
        $this->_last_insert_id = $this->conn->lastInsertId();

        return $this->_last_insert_id;
    }

    /*
     *
     * model_name: returned binding type
     * */
    function preQuery($table_name, array $conditions = [], $model_name=null){
        $values = [];
        $bind_data = [];
        foreach ($conditions as $key => $value){
            $values[] = "{$key}=:{$key}";
            $bind_data[":{$key}"] = $value;
        }
        $sql = "SELECT * FROM {$table_name} WHERE " . implode(' and ', $values);
        syslog(LOG_DEBUG, "sql executed: {$sql}");
        $this->_stmt = $this->conn->prepare($sql);
        $this->_bind($bind_data);
        $this->_stmt->execute();
        if ($model_name != null)
            $this->_stmt->setFetchMode(PDO::FETCH_CLASS, $model_name);

        return $this->_stmt;
    }

    function query($table_name, array $conditions = [], $model_name=null){
        $stmt =  $this->preQuery($table_name, $conditions, $model_name);
        return $stmt->fetch();
    }

    function queryMultiple($table_name, array $conditions = [1 => 1]){
        $stmt =  $this->preQuery($table_name, $conditions);

        return $stmt->fetchAll();
    }

    function getInsertId(){
        return $this->_last_insert_id;
    }

    function update($table, array $param = [], array $conditions){
        $data = array_merge($this->_data, $param);
        $bind_temp = [];
        $bind_data = [];
        $bind_conditions = [];
        foreach($data as $key=>$value){
            $bind_temp[] = "{$key} = :{$key}";
            $bind_data[":{$key}"] = $value;
        }

        foreach ($conditions as $key=>$value){
            $bind_conditions[] = "{$key}='{$conditions[$key]}'";
        }

        $sql = "UPDATE {$table} SET " . implode(',', $bind_temp) . " WHERE ". implode(', ', $bind_conditions);
        $this->_stmt = $this->conn->prepare($sql);
        $this->_bind($bind_data);
        $this->_stmt->execute();
//        echo $this->_stmt->errorInfo();
        return $this->_stmt->rowCount();
    }

    function delete($table, array $conditions){

        foreach ($conditions as $key=>$value){
            $bind_conditions[] = "{$key}='{$conditions[$key]}'";
        }

        $sql = "DELETE FROM {$table} WHERE ". implode(', ', $bind_conditions);
        $this->_stmt = $this->conn->prepare($sql);
        $this->_stmt->execute();
        return $this->_stmt->rowCount();
    }
}
?>