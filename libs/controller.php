<?php

/**
 * Created by PhpStorm.
 * User: shanghengwu
 * Date: 16/03/2017
 * Time: 10:53 AM
 */
class Controller
{
    protected $data; //contain all details from controller to view
    protected $model; //to access model object
    protected $params; //parameters received from the request

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @return mixed
     */
    public function getModel()
    {
        return $this->model;
    }
    /**
     * @return mixed
     */
    public function getParams()
    {
        return $this->params;
    }


    public function __construct($data = array()){
        $this->data = $data;
        $this->params = App::getRouter()->getParams();
    }
}