<?php
/**
 * Created by PhpStorm.
 * User: shanghengwu
 * Date: 19/03/2017
 * Time: 10:04 PM
 */

include_once ("libs/model.php");

class User extends Model {
    protected $table_name = "users";

    public function getAllUsers(){
        $result = $this->pdo->queryMultiple($this->table_name, [ 1 => 1]);
        return $result;
    }

    public function getByLogin($login){
        $condition = ["login" => $login];

        syslog(LOG_INFO, "login: {$login}");
        $result = $this->pdo->query($this->table_name, $condition);

        if ( isset($result) ){
            return $result;
        }
        return false;
    }

    public function getByUsername($username){
        $conditions = [
            "login" => $username,
            "role" => "user"
        ];

        $result = $this->pdo->query($this->table_name, $conditions);
        if ( isset($result) ){
            return $result;
        }
        return false;
    }
    public function getById($id){
        $conditions = [
            "id" => $id
        ];

        $result = $this->pdo->query($this->table_name, $conditions);
        if ( isset($result) ){
            return $result;
        }
        return false;
    }

    public function update($data, $id = null){
        if (!isset($data['id']) ||
            !isset($data['login']) ||
            !isset($data['role']) ||
            !isset($data['is_active']) ){
            return false;
        }

        $id = (int)$id;
        $login = $data["login"];
        $role = $data["role"];
        $is_active = $data["is_active"];

        $params = [
            "login" => $login,
            "role" => $role,
            "is_active" => $is_active
        ];

        $conditions = [ "id" => $id ];
        $result = $this->pdo->update($this->table_name, $params, $conditions);

        return $result;
    }
    public function create($username, $password){
        $params = [
            "login" => $username,
            "password" => $password
        ];

        $result = $this->pdo->insert($this->table_name, $params);

        if ( isset($result) ){
            return $result;
        }

        return false;
    }

    public function delete($id){
        $conditions = [
            "id" => $id
        ];

        $result = $this->pdo->delete($this->table_name, $conditions);

        if ( isset($result) ){
            return $result;
        }

        return false;
    }

}