<?php
/**
 * Created by PhpStorm.
 * User: shanghengwu
 * Date: 19/03/2017
 * Time: 5:41 AM
 */

include_once (ROOT.DS."libs".DS."model.php");
class Message extends Model {

    private $table_name = "messages";

    public function save($data, $id = null){
        if (!isset($data['receiver_name']) ||
            !isset($data['receiver_email']) ||
            !isset($data['secret_key']) ||
            !isset($data['sender_id']) ||
            !isset($data['message'])){
            return false;
        }

        $id = (int)$id;
//        $receiver_name = $data["receiver_name"];
//        $receiver_email = $data["receiver_email"];
//        $sender_id = $data["sender_id"];
//        $secret_key = $data["secret_key"];
//        $message = $data["message"];

        $params = [
            "receiver_name" => $data[receiver_name],
            "receiver_email" => $data[receiver_email],
            "secret_key" => $data[secret_key],
            "sender_id" => $data[sender_id],
            "message" => $data[message]
        ];

        if ( !$id ){
            $result = $this->pdo->insert($this->table_name, $params);
            return $result;
        } else { // update existing record
            $conditions = [ "id" => $id];
            $this->pdo->update($this->table_name, $params, $conditions);
            return $id;
        }
    }

    public function getList(){
        $result = $this->pdo->queryMultiple($this->table_name, [1 => 1]);

        return $result;
    }

    public function getById($id){
        $condition = ["id" => $id];
        $result = $this->pdo->query($this->table_name, $condition);

        return $result;
    }

}