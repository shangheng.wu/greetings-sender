<?php

include_once("libs/router.php");

DEFINE("DS", DIRECTORY_SEPARATOR);
DEFINE("ROOT", $_SERVER["DOCUMENT_ROOT"]); // root path
DEFINE("VIEWS_PATH", ROOT.DS.'views');

require_once (ROOT.DS.'libs'.DS.'init.php');
require_once(ROOT.DS."libs".DS."app.php");
require_once(ROOT.DS."libs".DS."session.php");

//Session::setFlash("Test flash message");
session_start();
//try{
//    App::run($uri);
//} catch (Exception $ex){
//    Router::redirect('/');
//}
$uri = ($_SERVER['REQUEST_URI']);
App::run($uri);
